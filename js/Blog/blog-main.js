$(document).ready(function(){
	
	$('.post-content').find('video').each(function(i,e){
		// console.log($(e));
		$(this).wrap( "<div class='video-wrapper'></div>" );
		$(this).parent().parent().find('.video-wrapper').append('<div class="play-btn"></div>');
		$(this)[i].controls = false;
	})


	$('.post-content .video-wrapper .play-btn').click(function(){
			console.log(this);
			$(this).addClass('disabled');
			$(this).parent().find('video')[0].controls = true;
			$(this).parent().find('video')[0].play();

	})


	// POPULAR POST SLIDER 

	$('.blog-main .popular-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        dots: true,
        infinite: true,
        arrows: false
    });

	// Height whitepapers


	if( $(window).innerWidth() < 500 ){
		trimText('.breadcrumbs-nav span', 32)
	}
})

$(window).resize(function(){

	
})

$(window).load(function(){

	$('.ios .post-content').find('video').each(function(i,e){
		$(this)[i].load();
	})
})


function setWhitepapersListingHeight(){

	var setHeight = $('.whitepapers-wrapper').parent().height()
	$('.white-papers .image').css('height', setHeight)

}


waitForLoad(".white-papers .hs-cta-wrapper",".cta_button",setWhitepapersListingHeight );
              


function trimText(wrapper, trimTo){

	var text = $(wrapper).text();
	var shortText = jQuery.trim(text).substring(0, trimTo).trim(this) + "...";
	$(wrapper).html(shortText)

}


// 

