waitForLoad("[id^=hs_form_target], [id^=hbspt-form]", 'form', function() {
    // add focus & blur effects
    var textInputs = $('.hs-form-field:not(.hs-fieldtype-radio):not(.hs-fieldtype-checkbox) input, .hs-form-field:not(.hs-fieldtype-radio):not(.hs-fieldtype-checkbox) textarea');
	textInputs.each(function() {
		if ($(this).val()) {
			$(this).parents('.hs-form-field').addClass('focus');
		}
	});

	textInputs.focus(function() {
		$(this).parents('.hs-form-field').addClass('focus');
	})

	textInputs.focusout(function() {
		if ($(this).val() === "") {
			$(this).parents('.hs-form-field').removeClass('focus');
		}
	})

	// file input
	if ($('.hs-fieldtype-file')) {
	    $('.hs-fieldtype-file .input').each(function () {
	      $(this).prepend('<p class="file-placeholder">Upload file</p>');
	    });
	}

	$('.hs-fieldtype-file input').on('change', function () {
		var fileName = $(this)[0].files[0].name;
		var placeholder = $(this).siblings('.file-placeholder');
		placeholder.text(fileName);
	});

	// move progressive subscribe checkbox to the bottom
	
	$('.hs-dependent-field').each(function () {
		if ($(this).find('.hs_subscribed_for_marketing_news1')) {
		  var fieldToMove = $(this).find('.hs_subscribed_for_marketing_news1');
		  if ($(this).parents('fieldset').length > 0) {
		    var lastFieldset = $(this).parents('form').find('fieldset').last();
		    lastFieldset.after(fieldToMove);
		  } else {
		    var lastField = $(this).parents('form').find('.hs-form-field').last();
		    lastField.after(fieldToMove);
		  }
		}
	});

	$('.hs-dependent-field input, .hs-dependent-field select').on('change', function() {
		if ($(this).parents('.hs-dependent-field').find('.hs_subscribed_for_marketing_news1')) {
			var fieldToMove = $(this).parents('.hs-dependent-field').find('.hs_subscribed_for_marketing_news1');
			if ($(this).parents('fieldset').length > 0) {
				var lastFieldset = $(this).parents('form').find('fieldset').last();
				lastFieldset.after(fieldToMove);
			} else {
				var lastField = $(this).parents('form').find('.hs-form-field').last();
				lastField.after(fieldToMove);
			}
		}
	})
});