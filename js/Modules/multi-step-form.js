waitForLoad("[id^=hs_form_target], [id^=hbspt-form]", 'form', function(elem) {
  var form = elem;
  var findStep = elem.find('.hs-field-desc').text();
  var nextStepText = elem.find('.hs-field-desc').first().text().split(',')[2];
  var prevStepText = elem.find('.hs-field-desc').first().text().split(',')[3];

  if (findStep.indexOf('step') >= 0) {
    var formField = '.hs-form-field';
    if (form.find('fieldset').length > 0) {
      formField = 'fieldset';
    }
    form.addClass('multi-step-form');
    var stepNum = 0;
    var stepTitle = "";
    elem.find('.hs-field-desc').each(function() {
      $(this).css('display', 'none');
      if ($(this).text().indexOf('step') >= 0) {
        stepNum++;
        if (typeof $(this).text().split(',')[1] !== "undefined") {
          stepTitle = $(this).text().split(',')[1];
        } else {
          stepTitle = "";
        }
        $(this).parents('form').append('<div class="form-step" data-step=' + stepNum + '><div class="step-title"><h3>' + stepTitle + '</h3></div></div>');
      }
      form.find('.form-step[data-step=' + stepNum + ']').append($(this).parents(formField));
    })
    

    var i = 0
    form.find('.form-step').each(function(i) {
      $(this).append('<div class="step-control"></div>');

      if (i > 0 && i < stepNum) {
        $(this).find('.step-control').append('<a class="step-control__button step-control__button--prev">' + prevStepText + '</a>');
      }

      if (i + 1 < stepNum) {
        $(this).find('.step-control').append('<a class="step-control__button step-control__button--next">' + nextStepText + '</a>');
      }
    });

    // append legal consent to last step
    form.find('.form-step[data-step=' + stepNum + '] .step-control').prepend(form.find('.legal-consent-container'));

    // append submit to last step
    form.find('.form-step[data-step=' + stepNum + '] .step-control').append(form.find('.hs-submit')); 

    validateStep();
  }
});

function validateStep() {

  // go to next step
  $('.multi-step-form .step-control__button--next').on('click', function() {
    var totalSteps = $(this).parents('form').find('.form-step').length;
    var activeStep = $(this).parents('.form-step');

    activeStep.find('[required]').each(function() {
      $(this).focus();
      $(this).blur();
    });

    setTimeout(function() {
      var errorCount = activeStep.find('.error').length;
      if (activeStep.data('step') > 0 && errorCount === 0) {
        activeStep.css('display', 'none');
        activeStep.next('.form-step').css('display', 'block');

        if (activeStep.data('step') + 1 === totalSteps) {
          activeStep.next('.form-step').addClass('last-active');
        }
      }
    }, 10);
  });

  // go to previous step
  $('.multi-step-form .step-control__button--prev').on('click', function() {
    var activeStep = $(this).parents('.form-step');
    activeStep.css('display', 'none');
    activeStep.prev('.form-step').css('display', 'block');
    $('.form-step').removeClass('last-active');
  });

  // submit form
  $('.multi-step-form input[type=submit]').on('click', function(e) {
      if ($(this).parents('.form-step').hasClass('last-active') == false) {
         e.preventDefault();
      }
  });
}