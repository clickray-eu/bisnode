'use strict';

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

// file: forms.js
waitForLoad("[id^=hs_form_target], [id^=hbspt-form]", 'form', function () {
  // add focus & blur effects
  var textInputs = $('.hs-form-field:not(.hs-fieldtype-radio):not(.hs-fieldtype-checkbox) input, .hs-form-field:not(.hs-fieldtype-radio):not(.hs-fieldtype-checkbox) textarea');
  textInputs.each(function () {
    if ($(this).val()) {
      $(this).parents('.hs-form-field').addClass('focus');
    }
  });

  textInputs.focus(function () {
    $(this).parents('.hs-form-field').addClass('focus');
  });

  textInputs.focusout(function () {
    if ($(this).val() === "") {
      $(this).parents('.hs-form-field').removeClass('focus');
    }
  });

  // file input
  if ($('.hs-fieldtype-file')) {
    $('.hs-fieldtype-file .input').each(function () {
      $(this).prepend('<p class="file-placeholder">Upload file</p>');
    });
  }

  $('.hs-fieldtype-file input').on('change', function () {
    var fileName = $(this)[0].files[0].name;
    var placeholder = $(this).siblings('.file-placeholder');
    placeholder.text(fileName);
  });

  // move progressive subscribe checkbox to the bottom

  $('.hs-dependent-field').each(function () {
    if ($(this).find('.hs_subscribed_for_marketing_news1')) {
      var fieldToMove = $(this).find('.hs_subscribed_for_marketing_news1');
      if ($(this).parents('fieldset').length > 0) {
        var lastFieldset = $(this).parents('form').find('fieldset').last();
        lastFieldset.after(fieldToMove);
      } else {
        var lastField = $(this).parents('form').find('.hs-form-field').last();
        lastField.after(fieldToMove);
      }
    }
  });

  $('.hs-dependent-field input, .hs-dependent-field select').on('change', function () {
    if ($(this).parents('.hs-dependent-field').find('.hs_subscribed_for_marketing_news1')) {
      var fieldToMove = $(this).parents('.hs-dependent-field').find('.hs_subscribed_for_marketing_news1');
      if ($(this).parents('fieldset').length > 0) {
        var lastFieldset = $(this).parents('form').find('fieldset').last();
        lastFieldset.after(fieldToMove);
      } else {
        var lastField = $(this).parents('form').find('.hs-form-field').last();
        lastField.after(fieldToMove);
      }
    }
  });
});
// end file: forms.js

// file: global.js
$(document).ready(function () {

  detectBrowser();
});

function waitForLoad(wrapper, element, callback) {
  if ($(wrapper).length > 0) {
    $(wrapper).each(function (i, el) {
      var waitForLoad = setInterval(function () {
        if ($(el).length == $(el).find(element).length) {
          clearInterval(waitForLoad);
          callback($(el), $(el).find(element));
        }
      }, 50);
    });
  }
}

function detectBrowser() {

  var iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
  if (iOS) $('body').addClass('ios');

  if (/MSIE 9/i.test(navigator.userAgent) || /rv:11.0/i.test(navigator.userAgent) || /MSIE 10/i.test(navigator.userAgent) || /Edge\/12./i.test(navigator.userAgent) || window.navigator.userAgent.indexOf("Edge") > -1) {
    $('body').addClass('ms-browser');
  }

  if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
    $('body').addClass('ff');
  }
}
// end file: global.js

// file: header.js

$(document).ready(function () {

  activeMenuElement();
});
function activeMenuElement() {

  var x = location.href;
  var c = x.split("/");
  c = c[c.length - 1];

  // console.log(c)

  $('.main-header .tag-nav a').each(function () {
    if ($(this).attr('data-name') == c) $(this).parent().addClass('active');
  });

  $('.sidebar-menu-area .tag-nav a').each(function () {
    if ($(this).attr('data-name') == c) $(this).addClass('active');
  });

  $('.filters-nav a').each(function () {
    if ($(this).attr('data-name') == c) $(this).addClass('active');
  });
}

// end file: header.js

// file: sidebar-menu.js
$(document).ready(function () {

  // $('.sidebar-menu .main-menu').click(function(){

  // 	$(this).find('.hamburger').toggleClass('active')
  // })


});
// end file: sidebar-menu.js

// file: Modules/multi-step-form.js
waitForLoad("[id^=hs_form_target], [id^=hbspt-form]", 'form', function (elem) {
  var form = elem;
  var findStep = elem.find('.hs-field-desc').text();
  var nextStepText = elem.find('.hs-field-desc').first().text().split(',')[2];
  var prevStepText = elem.find('.hs-field-desc').first().text().split(',')[3];

  if (findStep.indexOf('step') >= 0) {
    var formField = '.hs-form-field';
    if (form.find('fieldset').length > 0) {
      formField = 'fieldset';
    }
    form.addClass('multi-step-form');
    var stepNum = 0;
    var stepTitle = "";
    elem.find('.hs-field-desc').each(function () {
      $(this).css('display', 'none');
      if ($(this).text().indexOf('step') >= 0) {
        stepNum++;
        if (typeof $(this).text().split(',')[1] !== "undefined") {
          stepTitle = $(this).text().split(',')[1];
        } else {
          stepTitle = "";
        }
        $(this).parents('form').append('<div class="form-step" data-step=' + stepNum + '><div class="step-title"><h3>' + stepTitle + '</h3></div></div>');
      }
      form.find('.form-step[data-step=' + stepNum + ']').append($(this).parents(formField));
    });

    var i = 0;
    form.find('.form-step').each(function (i) {
      $(this).append('<div class="step-control"></div>');

      if (i > 0 && i < stepNum) {
        $(this).find('.step-control').append('<a class="step-control__button step-control__button--prev">' + prevStepText + '</a>');
      }

      if (i + 1 < stepNum) {
        $(this).find('.step-control').append('<a class="step-control__button step-control__button--next">' + nextStepText + '</a>');
      }
    });

    // append legal consent to last step
    form.find('.form-step[data-step=' + stepNum + '] .step-control').prepend(form.find('.legal-consent-container'));

    // append submit to last step
    form.find('.form-step[data-step=' + stepNum + '] .step-control').append(form.find('.hs-submit'));

    validateStep();
  }
});

function validateStep() {

  // go to next step
  $('.multi-step-form .step-control__button--next').on('click', function () {
    var totalSteps = $(this).parents('form').find('.form-step').length;
    var activeStep = $(this).parents('.form-step');

    activeStep.find('[required]').each(function () {
      $(this).focus();
      $(this).blur();
    });

    setTimeout(function () {
      var errorCount = activeStep.find('.error').length;
      if (activeStep.data('step') > 0 && errorCount === 0) {
        activeStep.css('display', 'none');
        activeStep.next('.form-step').css('display', 'block');

        if (activeStep.data('step') + 1 === totalSteps) {
          activeStep.next('.form-step').addClass('last-active');
        }
      }
    }, 10);
  });

  // go to previous step
  $('.multi-step-form .step-control__button--prev').on('click', function () {
    var activeStep = $(this).parents('.form-step');
    activeStep.css('display', 'none');
    activeStep.prev('.form-step').css('display', 'block');
    $('.form-step').removeClass('last-active');
  });

  // submit form
  $('.multi-step-form input[type=submit]').on('click', function (e) {
    if ($(this).parents('.form-step').hasClass('last-active') == false) {
      e.preventDefault();
    }
  });
}
// end file: Modules/multi-step-form.js

// file: Modules/popup.js
/* ========================================================================
 * Bootstrap: modal.js v3.3.7
 * http://getbootstrap.com/javascript/#modals
 * ========================================================================
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */

+function ($) {
  'use strict';

  // MODAL CLASS DEFINITION
  // ======================

  var Modal = function Modal(element, options) {
    this.options = options;
    this.$body = $(document.body);
    this.$element = $(element);
    this.$dialog = this.$element.find('.modal-dialog');
    this.$backdrop = null;
    this.isShown = null;
    this.originalBodyPad = null;
    this.scrollbarWidth = 0;
    this.ignoreBackdropClick = false;

    if (this.options.remote) {
      this.$element.find('.modal-content').load(this.options.remote, $.proxy(function () {
        this.$element.trigger('loaded.bs.modal');
      }, this));
    }
  };

  Modal.VERSION = '3.3.7';

  Modal.TRANSITION_DURATION = 300;
  Modal.BACKDROP_TRANSITION_DURATION = 150;

  Modal.DEFAULTS = {
    backdrop: true,
    keyboard: true,
    show: true
  };

  Modal.prototype.toggle = function (_relatedTarget) {
    return this.isShown ? this.hide() : this.show(_relatedTarget);
  };

  Modal.prototype.show = function (_relatedTarget) {
    var that = this;
    var e = $.Event('show.bs.modal', { relatedTarget: _relatedTarget });

    this.$element.trigger(e);

    if (this.isShown || e.isDefaultPrevented()) return;

    this.isShown = true;

    this.checkScrollbar();
    this.setScrollbar();
    this.$body.addClass('modal-open');

    this.escape();
    this.resize();

    this.$element.on('click.dismiss.bs.modal', '[data-dismiss="modal"]', $.proxy(this.hide, this));

    this.$dialog.on('mousedown.dismiss.bs.modal', function () {
      that.$element.one('mouseup.dismiss.bs.modal', function (e) {
        if ($(e.target).is(that.$element)) that.ignoreBackdropClick = true;
      });
    });

    this.backdrop(function () {
      var transition = $.support.transition && that.$element.hasClass('fade');

      if (!that.$element.parent().length) {
        that.$element.appendTo(that.$body); // don't move modals dom position
      }

      that.$element.show().scrollTop(0);

      that.adjustDialog();

      if (transition) {
        that.$element[0].offsetWidth; // force reflow
      }

      that.$element.addClass('in');

      that.enforceFocus();

      var e = $.Event('shown.bs.modal', { relatedTarget: _relatedTarget });

      transition ? that.$dialog // wait for modal to slide in
      .one('bsTransitionEnd', function () {
        that.$element.trigger('focus').trigger(e);
      }).emulateTransitionEnd(Modal.TRANSITION_DURATION) : that.$element.trigger('focus').trigger(e);
    });
  };

  Modal.prototype.hide = function (e) {
    if (e) e.preventDefault();

    e = $.Event('hide.bs.modal');

    this.$element.trigger(e);

    if (!this.isShown || e.isDefaultPrevented()) return;

    this.isShown = false;

    this.escape();
    this.resize();

    $(document).off('focusin.bs.modal');

    this.$element.removeClass('in').off('click.dismiss.bs.modal').off('mouseup.dismiss.bs.modal');

    this.$dialog.off('mousedown.dismiss.bs.modal');

    $.support.transition && this.$element.hasClass('fade') ? this.$element.one('bsTransitionEnd', $.proxy(this.hideModal, this)).emulateTransitionEnd(Modal.TRANSITION_DURATION) : this.hideModal();
  };

  Modal.prototype.enforceFocus = function () {
    $(document).off('focusin.bs.modal') // guard against infinite focus loop
    .on('focusin.bs.modal', $.proxy(function (e) {
      if (document !== e.target && this.$element[0] !== e.target && !this.$element.has(e.target).length) {
        this.$element.trigger('focus');
      }
    }, this));
  };

  Modal.prototype.escape = function () {
    if (this.isShown && this.options.keyboard) {
      this.$element.on('keydown.dismiss.bs.modal', $.proxy(function (e) {
        e.which == 27 && this.hide();
      }, this));
    } else if (!this.isShown) {
      this.$element.off('keydown.dismiss.bs.modal');
    }
  };

  Modal.prototype.resize = function () {
    if (this.isShown) {
      $(window).on('resize.bs.modal', $.proxy(this.handleUpdate, this));
    } else {
      $(window).off('resize.bs.modal');
    }
  };

  Modal.prototype.hideModal = function () {
    var that = this;
    this.$element.hide();
    this.backdrop(function () {
      that.$body.removeClass('modal-open');
      that.resetAdjustments();
      that.resetScrollbar();
      that.$element.trigger('hidden.bs.modal');
    });
  };

  Modal.prototype.removeBackdrop = function () {
    this.$backdrop && this.$backdrop.remove();
    this.$backdrop = null;
  };

  Modal.prototype.backdrop = function (callback) {
    var that = this;
    var animate = this.$element.hasClass('fade') ? 'fade' : '';

    if (this.isShown && this.options.backdrop) {
      var doAnimate = $.support.transition && animate;

      this.$backdrop = $(document.createElement('div')).addClass('modal-backdrop ' + animate).appendTo(this.$body);

      this.$element.on('click.dismiss.bs.modal', $.proxy(function (e) {
        if (this.ignoreBackdropClick) {
          this.ignoreBackdropClick = false;
          return;
        }
        if (e.target !== e.currentTarget) return;
        this.options.backdrop == 'static' ? this.$element[0].focus() : this.hide();
      }, this));

      if (doAnimate) this.$backdrop[0].offsetWidth; // force reflow

      this.$backdrop.addClass('in');

      if (!callback) return;

      doAnimate ? this.$backdrop.one('bsTransitionEnd', callback).emulateTransitionEnd(Modal.BACKDROP_TRANSITION_DURATION) : callback();
    } else if (!this.isShown && this.$backdrop) {
      this.$backdrop.removeClass('in');

      var callbackRemove = function callbackRemove() {
        that.removeBackdrop();
        callback && callback();
      };
      $.support.transition && this.$element.hasClass('fade') ? this.$backdrop.one('bsTransitionEnd', callbackRemove).emulateTransitionEnd(Modal.BACKDROP_TRANSITION_DURATION) : callbackRemove();
    } else if (callback) {
      callback();
    }
  };

  // these following methods are used to handle overflowing modals

  Modal.prototype.handleUpdate = function () {
    this.adjustDialog();
  };

  Modal.prototype.adjustDialog = function () {
    var modalIsOverflowing = this.$element[0].scrollHeight > document.documentElement.clientHeight;

    this.$element.css({
      paddingLeft: !this.bodyIsOverflowing && modalIsOverflowing ? this.scrollbarWidth : '',
      paddingRight: this.bodyIsOverflowing && !modalIsOverflowing ? this.scrollbarWidth : ''
    });
  };

  Modal.prototype.resetAdjustments = function () {
    this.$element.css({
      paddingLeft: '',
      paddingRight: ''
    });
  };

  Modal.prototype.checkScrollbar = function () {
    var fullWindowWidth = window.innerWidth;
    if (!fullWindowWidth) {
      // workaround for missing window.innerWidth in IE8
      var documentElementRect = document.documentElement.getBoundingClientRect();
      fullWindowWidth = documentElementRect.right - Math.abs(documentElementRect.left);
    }
    this.bodyIsOverflowing = document.body.clientWidth < fullWindowWidth;
    this.scrollbarWidth = this.measureScrollbar();
  };

  Modal.prototype.setScrollbar = function () {
    var bodyPad = parseInt(this.$body.css('padding-right') || 0, 10);
    this.originalBodyPad = document.body.style.paddingRight || '';
    if (this.bodyIsOverflowing) this.$body.css('padding-right', bodyPad + this.scrollbarWidth);
  };

  Modal.prototype.resetScrollbar = function () {
    this.$body.css('padding-right', this.originalBodyPad);
  };

  Modal.prototype.measureScrollbar = function () {
    // thx walsh
    var scrollDiv = document.createElement('div');
    scrollDiv.className = 'modal-scrollbar-measure';
    this.$body.append(scrollDiv);
    var scrollbarWidth = scrollDiv.offsetWidth - scrollDiv.clientWidth;
    this.$body[0].removeChild(scrollDiv);
    return scrollbarWidth;
  };

  // MODAL PLUGIN DEFINITION
  // =======================

  function Plugin(option, _relatedTarget) {
    return this.each(function () {
      var $this = $(this);
      var data = $this.data('bs.modal');
      var options = $.extend({}, Modal.DEFAULTS, $this.data(), (typeof option === 'undefined' ? 'undefined' : _typeof(option)) == 'object' && option);

      if (!data) $this.data('bs.modal', data = new Modal(this, options));
      if (typeof option == 'string') data[option](_relatedTarget);else if (options.show) data.show(_relatedTarget);
    });
  }

  var old = $.fn.modal;

  $.fn.modal = Plugin;
  $.fn.modal.Constructor = Modal;

  // MODAL NO CONFLICT
  // =================

  $.fn.modal.noConflict = function () {
    $.fn.modal = old;
    return this;
  };

  // MODAL DATA-API
  // ==============

  $(document).on('click.bs.modal.data-api', '[data-toggle="modal"]', function (e) {
    var $this = $(this);
    var href = $this.attr('href');
    var $target = $($this.attr('data-target') || href && href.replace(/.*(?=#[^\s]+$)/, '')); // strip for ie7
    var option = $target.data('bs.modal') ? 'toggle' : $.extend({ remote: !/#/.test(href) && href }, $target.data(), $this.data());

    if ($this.is('a')) e.preventDefault();

    $target.one('show.bs.modal', function (showEvent) {
      if (showEvent.isDefaultPrevented()) return; // only register focus restorer if modal will actually get shown
      $target.one('hidden.bs.modal', function () {
        $this.is(':visible') && $this.trigger('focus');
      });
    });
    Plugin.call($target, option, this);
  });
}(jQuery);

// end file: Modules/popup.js

// file: Blog/blog-main.js
$(document).ready(function () {

  $('.post-content').find('video').each(function (i, e) {
    // console.log($(e));
    $(this).wrap("<div class='video-wrapper'></div>");
    $(this).parent().parent().find('.video-wrapper').append('<div class="play-btn"></div>');
    $(this)[i].controls = false;
  });

  $('.post-content .video-wrapper .play-btn').click(function () {
    console.log(this);
    $(this).addClass('disabled');
    $(this).parent().find('video')[0].controls = true;
    $(this).parent().find('video')[0].play();
  });

  // POPULAR POST SLIDER 

  $('.blog-main .popular-slider').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    dots: true,
    infinite: true,
    arrows: false
  });

  // Height whitepapers


  if ($(window).innerWidth() < 500) {
    trimText('.breadcrumbs-nav span', 32);
  }
});

$(window).resize(function () {});

$(window).load(function () {

  $('.ios .post-content').find('video').each(function (i, e) {
    $(this)[i].load();
  });
});

function setWhitepapersListingHeight() {

  var setHeight = $('.whitepapers-wrapper').parent().height();
  $('.white-papers .image').css('height', setHeight);
}

waitForLoad(".white-papers .hs-cta-wrapper", ".cta_button", setWhitepapersListingHeight);

function trimText(wrapper, trimTo) {

  var text = $(wrapper).text();
  var shortText = jQuery.trim(text).substring(0, trimTo).trim(this) + "...";
  $(wrapper).html(shortText);
}

// 


// end file: Blog/blog-main.js

// file: Landing/subscription_preferences.js

waitForLoad(".subscription-preferences .header_form", 'form', function ($wrapper, $element) {
  $element.find("[for='globalunsub']").remove();
});
waitForLoad(".subscription-preferences .footer_form", 'form', function ($wrapper, $element) {
  $element.find("[for='globalunsub']").remove();
});

// end file: Landing/subscription_preferences.js
//# sourceMappingURL=template.js.map
